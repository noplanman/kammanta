from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5 import QtWidgets
import kmt.model
import kmt.widgets.checklist_cw
import kmt.glob
import kmt.gtd_info

TITLE_STR = "Next Actions and Projects"


class NAsPrjs(QtWidgets.QWidget):
    focus_active_signal = QtCore.pyqtSignal(bool)

    def __init__(self):
        super().__init__()

        hbox_l3 = QtWidgets.QHBoxLayout()
        self.setLayout(hbox_l3)

        self.next_actions_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.na_files)
        hbox_l3.addWidget(self.next_actions_column)
        self.next_actions_column.setWhatsThis(kmt.gtd_info.NA_STR)
        self.next_actions_column.focus_active_signal.connect(self.focus_active_signal.emit)

        self.projects_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.prj_fds)
        hbox_l3.addWidget(self.projects_column)
        self.projects_column.setWhatsThis(kmt.gtd_info.PRJ_STR)
        self.projects_column.focus_active_signal.connect(self.focus_active_signal.emit)

        self.update_gui()

    # overridden
    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        pass
        # self.update_gui()
        # -removed for now since this has been seen (only once) to cause an infinite loop

    def add_fsw_dirs(self):
        main_dir_str = kmt.model.na_files.get_path()
        kmt.glob.FswSingleton.get().addPath(main_dir_str)

        """
        na_path_dir_str = kmt.model.na_files.get_path()
        kmt.glob.FswSingleton.get().addPath(na_path_dir_str)
        prj_path_dir_str = kmt.model.prj_fds.get_path()
        kmt.glob.FswSingleton.get().addPath(prj_path_dir_str)
        """

    def update_gui(self):
        self.add_fsw_dirs()

        self.next_actions_column.update_gui_and_ids()
        self.projects_column.update_gui_and_ids()
