



# Backend

## Directories + .desktop files

Used for
* projects

## Text files with lines/rows

Used for
* Contexts + Next Actions
* Agendas + Agenda Items

## Unstructured files

If text: With freeform writing

Can also be images, or any file format (or dirs?)

Used for
* notes+inbox
* contacts
* tickler


# GUI

## Updates

## Signals



