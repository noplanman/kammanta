import functools
import logging
import os
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets

import kmt.glob
import kmt.model
import kmt.widgets.path_sel_dlg


class TextListWidget(QtWidgets.QWidget):
    # new_list_row_added_signal = QtCore.pyqtSignal(str)
    # -the string has the na id

    def __init__(self, i_title: str, i_collection_ptr: kmt.model.TreeBranchI):
        super().__init__()
        self.collection_ptr = i_collection_ptr
        self.updating_gui_bool = False

        vbox_l2 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l2)

        # ..or this
        self.file_selection_qlw = QtWidgets.QListWidget()
        self.file_selection_qlw.currentTextChanged.connect(self.on_list_file_selection_text_changed)

        self.open_file_selection_file_qpb = QtWidgets.QPushButton("Open")
        self.open_file_selection_file_qpb.clicked.connect(self.open_coll)

        self.add_new_coll_qle = QtWidgets.QLineEdit()
        self.add_new_coll_qle.setPlaceholderText("placeholder text ----")
        # -perhaps allow customization of this, for example in settings.ini (globally) or in each context file
        self.add_new_coll_qpb = QtWidgets.QPushButton("Add new")
        self.add_new_coll_qpb.clicked.connect(self.on_add_new_coll_clicked)  # -------------------------------

    def on_list_file_selection_text_changed(self, i_new_text: str):
        if self.updating_gui_bool:
            return
        self.collection_ptr.set_active_item(i_new_text)
        self.update_gui()

    def open_coll(self):
        file_path_str = self.collection_ptr.get_active_item().get_path()
        kmt.model.open_default_external_application(file_path_str)

    def on_add_new_coll_clicked(self):
        self.collection_ptr.add_new_item(self.add_new_coll_qle.text())
        # TODO: Signal ___________ self.update_gui_item_list()

    def update_gui(self):
        self.updating_gui_bool = True

        # coll_list = [f for f in self.collection_ptr.get_all_subn() if f.get_name().endswith(".txt")]
        coll_list = self.collection_ptr.get_all_items()
        coll_name_list = [coll_item.get_name() for coll_item in coll_list]
        if self.collection_ptr.get_name() == kmt.glob.AGENDAS_DIR_STR:
            self.file_selection_qlw.clear()
            self.file_selection_qlw.addItems(coll_name_list)
            # if self.file_selection_qlw.currentRow() != -1:
            #   current_file_name_str = self.file_selection_qlw.item(self.file_selection_qlw.currentRow())
        else:
            self.file_selection_qcb.clear()
            self.file_selection_qcb.addItems(coll_name_list)
            # current_file_name_str = self.file_selection_qcb.currentText()

        if self.collection_ptr.get_active_item():
            self.list_area_qsw.setCurrentWidget(self.item_list_widget)
        else:
            self.list_area_qsw.setCurrentWidget(self.no_list_message_qll)

        self.update_gui_item_list()

        self.updating_gui_bool = False

