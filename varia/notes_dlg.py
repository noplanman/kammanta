
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
import sys
import time


class NotesDlg(QtWidgets.QDialog):
    """
    Inspiration: Answer by lou here:
    https://stackoverflow.com/questions/18196799/how-can-i-show-a-pyqt-modal-dialog-and-get-data-out-of-its-controls-once-its-clo
    """
    def __init__(self, i_parent = None):
        super(NotesDlg, self).__init__(i_parent)

        vbox = QtWidgets.QVBoxLayout(self)

        """
        self.description_qll = QtWidgets.QLabel(Notes)
        vbox.addWidget(self.description_qll)
        """

        self.text_edit = QtWidgets.QPlainTextEdit(self)
        vbox.addWidget(self.text_edit)

        self.button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
            QtCore.Qt.Horizontal,
            self
        )
        vbox.addWidget(self.button_box)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        # -accept and reject are "slots" built into Qt

    @staticmethod
    def get_notes_dialog() -> str:
        dialog = NotesDlg()
        dialog_result = dialog.exec_()
        if dialog_result == QtWidgets.QDialog.Accepted:
            return dialog.text_edit.toPlainText()
        return ""


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    string_content_str = NotesDlg.get_notes_dialog()
    print("String: " + string_content_str)
    sys.exit(app.exec_())
