
#### ***Please note:*** This is a prototype in early development

## Kammanta

Kammanta is a **productivity** and **life organizing** application inspired by the GTD© methodology,
and with two features that set it apart:
* Data is **stored as text files and directories** (the actual names of files and dirs are used)
* The application is ***free software*** (free as in freedom) which means that the source code is
open, and you can copy and create your own application if you want

### Installation

The application is developed on Ubuntu. Other platforms have not been tested but should work on other Linux-based systems and on MacOS. (Windows is the lease certain, but I hope to add Windows support in the future) 

#### Requirements

* Python 3.7
* PyQt 5.14 (and PyQt5-sip)

`sudo pip3 install -r requirements.txt`

#### Running from source

`python3.7 kammanta.py`

### Screenshots

#### Video

