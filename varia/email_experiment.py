from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtWebEngineWidgets
# example: https://github.com/smoqadam/PyFladesk/blob/master/pyfladesk/__init__.py
import logging
import subprocess
import os
import datetime
import time
import kmt.model
import functools


class EmailExpMain(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        hbox_l2 = QtWidgets.QHBoxLayout()
        self.setLayout(hbox_l2)

        self.engine_view = QtWebEngineWidgets.QWebEngineView(self)
        hbox_l2.addWidget(self.engine_view, stretch=1)

        self.page = QtWebEngineWidgets.QWebEnginePage()
        self.engine_view.setPage(self.page)
        self.page.load(
            QtCore.QUrl("https://mail.disroot.org/")
        )
        # -please note that this needs to be done after setPage

        # Tickler

        self.unnamed = QtWidgets.QLabel("other side 50% of width")
        hbox_l2.addWidget(self.unnamed, stretch=1)

    def update_gui(self):
        pass
