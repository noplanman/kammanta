import logging
import os
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import kmt.model
import kmt.widgets.checklist_cw
import kmt.widgets.calendar_input_dlg
import kmt.glob
import kmt.gtd_info
import kmt.widgets.input_cw
import kmt.widgets.path_sel_dlg

TITLE_STR = "Processing"


class Processing(QtWidgets.QWidget):
    close_widget_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()

        # self.counter_int = 0  # -for next button

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        # Topmost widgets

        top_hbox_l2 =  QtWidgets.QHBoxLayout()
        vbox_l1.addLayout(top_hbox_l2)

        self.title_qll = QtWidgets.QLabel("Processing")
        top_hbox_l2.addWidget(self.title_qll)
        self.title_qll.setFont(kmt.glob.get_title_font(True))

        # Main division of (1) source file and (2) processing..

        # .. left side

        main_hbox_l2 = QtWidgets.QHBoxLayout()
        vbox_l1.addLayout(main_hbox_l2)

        new_font = QtGui.QFont()
        new_font.setPointSize(14)

        left_vbox_l3 = QtWidgets.QVBoxLayout()
        main_hbox_l2.addLayout(left_vbox_l3)

        self.source_id_qll = QtWidgets.QLabel()
        left_vbox_l3.addWidget(self.source_id_qll)  # , stretch=1
        self.source_id_qll.setWordWrap(True)

        hbox_l4 = QtWidgets.QHBoxLayout()
        left_vbox_l3.addLayout(hbox_l4)

        self.delete_source_on_exit_qcb = QtWidgets.QCheckBox("del on close/next")
        hbox_l4.addWidget(self.delete_source_on_exit_qcb)
        self.delete_source_on_exit_qcb.setChecked(False)

        self.first_qpb = QtWidgets.QPushButton("&First")
        hbox_l4.addWidget(self.first_qpb)
        self.first_qpb.clicked.connect(self.on_first_clicked)
        self.first_qpb.setFont(kmt.glob.get_title_font(True))

        """
        self.next_qpb = QtWidgets.QPushButton("&Next")
        hbox_l4.addWidget(self.next_qpb)
        self.next_qpb.clicked.connect(self.on_next_clicked)
        self.next_qpb.setFont(gtd.gtd_global.get_title_font(True))
        """

        self.close_qpb = QtWidgets.QPushButton("&Close")
        hbox_l4.addWidget(self.close_qpb)
        self.close_qpb.clicked.connect(self.on_close_clicked)
        self.close_qpb.setFont(kmt.glob.get_title_font(True))

        self.left_qsw = QtWidgets.QStackedWidget()
        left_vbox_l3.addWidget(self.left_qsw)

        self.source_qpte = QtWidgets.QPlainTextEdit()
        self.left_qsw.addWidget(self.source_qpte)
        self.source_qpte.setFont(new_font)
        self.source_qpte.setReadOnly(True)

        self.source_image_qll = QtWidgets.QLabel()
        self.left_qsw.addWidget(self.source_image_qll)

        self.unknown_file_qll = QtWidgets.QLabel("No preview for this file type")
        self.left_qsw.addWidget(self.unknown_file_qll)

        # ..right side..

        right_vbox_l3 = QtWidgets.QVBoxLayout()
        main_hbox_l2.addLayout(right_vbox_l3)

        hbox_l4 = QtWidgets.QHBoxLayout()
        right_vbox_l3.addLayout(hbox_l4)

        self.actions_qbg = QtWidgets.QButtonGroup()
        self.actions_qbg.setExclusive(True)
        self.actions_qbg.buttonClicked.connect(self.on_action_button_clicked)

        actions_hbox_l4 = QtWidgets.QHBoxLayout()
        right_vbox_l3.addLayout(actions_hbox_l4)

        self.trash_qpb = QtWidgets.QPushButton("Trash")
        actions_hbox_l4.addWidget(self.trash_qpb)
        self.trash_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.trash_qpb)

        self.ref_qpb = QtWidgets.QPushButton("Ref")
        actions_hbox_l4.addWidget(self.ref_qpb)
        self.ref_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.ref_qpb)

        self.incubate_qpb = QtWidgets.QPushButton("Incubate")
        actions_hbox_l4.addWidget(self.incubate_qpb)
        self.incubate_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.incubate_qpb)

        self.do_qpb = QtWidgets.QPushButton("Do")
        actions_hbox_l4.addWidget(self.do_qpb)
        self.do_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.do_qpb)

        actions_hbox_l4 = QtWidgets.QHBoxLayout()
        right_vbox_l3.addLayout(actions_hbox_l4)

        self.na_qpb = QtWidgets.QPushButton("New NA")
        actions_hbox_l4.addWidget(self.na_qpb)
        self.na_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.na_qpb)

        self.prj_qpb = QtWidgets.QPushButton("New Prj")
        actions_hbox_l4.addWidget(self.prj_qpb)
        self.prj_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.prj_qpb)

        self.defer_qpb = QtWidgets.QPushButton("Defer")
        actions_hbox_l4.addWidget(self.defer_qpb)
        self.defer_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.defer_qpb)

        self.delegate_qpb = QtWidgets.QPushButton("Delegate")
        actions_hbox_l4.addWidget(self.delegate_qpb)
        self.delegate_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.delegate_qpb)


        self.right_qsw = QtWidgets.QStackedWidget()
        right_vbox_l3.addWidget(self.right_qsw)

        # ..widgets on the stack

        self.empty_qll = QtWidgets.QLabel("Please select a processing type")
        self.right_qsw.addWidget(self.empty_qll)

        self.do_widget = DoCw()
        self.right_qsw.addWidget(self.do_widget)

        self.trash_qll = QtWidgets.QLabel("Deleting")
        self.right_qsw.addWidget(self.trash_qll)

        self.defer_widget = DeferCw()
        self.right_qsw.addWidget(self.defer_widget)

        self.ref_dest = RefProcCw()
        self.right_qsw.addWidget(self.ref_dest)

        self.next_actions_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.na_files)
        self.right_qsw.addWidget(self.next_actions_column)

        self.projects_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.prj_fds)
        self.right_qsw.addWidget(self.projects_column)

        self.incubate_widget = IncubateProcCw()
        self.right_qsw.addWidget(self.incubate_widget)

        self.delegate_widget = DelegateCw()
        self.right_qsw.addWidget(self.delegate_widget)

        self.right_qsw.setCurrentWidget(self.empty_qll)

        # self.actions_qbg.

        self.update_gui()

    def on_first_clicked(self):
        inbox_item_list = kmt.model.inbox_dir.get_all_items()
        if len(inbox_item_list) > 0:
            inbox_item = inbox_item_list.pop(0)
            self.set_source_id(inbox_item.get_id())

    def on_next_clicked(self):
        pass

    def on_close_clicked(self):
        source_id_str = self.source_id_qll.text()
        if self.delete_source_on_exit_qcb.isChecked():
            kmt.model.inbox_dir.delete_item(source_id_str)

        self.get_dest_file_path()

        if self.do_qpb.isChecked():
            pass
        elif self.trash_qpb.isChecked():
            pass
        elif self.ref_qpb.isChecked():
            self.ref_dest.save_file()
        elif self.na_qpb.isChecked():
            pass
        elif self.prj_qpb.isChecked():
            pass
        elif self.defer_qpb.isChecked():
            pass
        elif self.delegate_qpb.isChecked():
            pass
        elif self.incubate_qpb.isChecked():
            self.incubate_widget.save_file()
        else:
            pass

        self.close_widget_signal.emit()

    def get_dest_file_path(self) -> str:
        return ""

    def get_source_id(self) -> str:
        source_id_str = self.source_id_qll.text()
        return source_id_str

    def set_source_id(self, i_id: str):
        self.source_id_qll.setText(i_id)

        inbox_item_obj = kmt.model.inbox_dir.get_item(i_id)
        source_path_str = inbox_item_obj.get_path()

        # Checking the type of file that we have
        dir_or_file_type = kmt.glob.get_df_type(source_path_str)

        if dir_or_file_type in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            try:
                with open(source_path_str, "r") as file:
                    self.source_qpte.setPlainText(file.read())
            except UnicodeDecodeError:
                pass
            self.left_qsw.setCurrentWidget(self.source_qpte)
        elif dir_or_file_type in (kmt.glob.EntityType.image_file,):
            pixmap = QtGui.QPixmap(source_path_str)
            pixmap = pixmap.scaled(
                340, 340,
                QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation
            )
            self.source_image_qll.setPixmap(pixmap)
            self.left_qsw.setCurrentWidget(self.source_image_qll)
        else:
            self.left_qsw.setCurrentWidget(self.unknown_file_qll)

        self.update_gui()

    def on_action_button_clicked(self, i_button):
        self.update_gui()

    # overridden
    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        pass
        # self.update_gui()
        # -removed for now since this has been seen (only once) to cause an infinite loop

    def update_gui(self):
        if self.do_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.do_widget)
        elif self.trash_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.trash_qll)
        elif self.ref_qpb.isChecked():
            source_id_str = self.source_id_qll.text()
            source_path_str = kmt.model.inbox_dir.get_item(source_id_str).get_path()
            self.ref_dest.set_source_path(source_path_str)
            self.right_qsw.setCurrentWidget(self.ref_dest)
        elif self.na_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.next_actions_column)
            self.next_actions_column.update_gui_and_ids()
        elif self.prj_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.projects_column)
            self.projects_column.update_gui_and_ids()
        elif self.defer_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.defer_widget)
        elif self.delegate_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.delegate_widget)
        elif self.incubate_qpb.isChecked():
            source_id_str = self.source_id_qll.text()
            self.incubate_widget.set_source(source_id_str)
            self.right_qsw.setCurrentWidget(self.incubate_widget)
        else:
            pass


# class DestWidget(QtWidgets.QWidget):


class DoCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.do_qll = QtWidgets.QLabel("Do if less than x minutes")
        vbox_l1.addWidget(self.do_qll)


class DeferCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.open_calendar_qpb = QtWidgets.QPushButton("Open Calendar")
        vbox_l1.addWidget(self.open_calendar_qpb)
        self.open_calendar_qpb.setFont(kmt.glob.get_title_font(True))
        self.open_calendar_qpb.clicked.connect(self.on_open_calendar_clicked)

        vbox_l1.addStretch(1)

        self.defer_qll = QtWidgets.QLabel("Schdule for another time")
        vbox_l1.addWidget(self.defer_qll)

        vbox_l1.addStretch(1)

    def on_open_calendar_clicked(self):
        calendar_str = kmt.glob.get_string_from_config(
            kmt.glob.SETTINGS_SECTION_EXTERNAL_TOOLS_STR,
            kmt.glob.SETTINGS_CALENDAR_STR,
            "https://calendar.google.com/calendar/r/week"
        )
        kmt.glob.launch_string(calendar_str)


class DelegateCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.open_email_qpb = QtWidgets.QPushButton("Open email")
        vbox_l1.addWidget(self.open_email_qpb)
        self.open_email_qpb.setFont(kmt.glob.get_title_font(True))
        self.open_email_qpb.clicked.connect(self.on_open_email_clicked)

        self.next_actions_widget = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.na_files)
        vbox_l1.addWidget(self.next_actions_widget)

        # TODO: making this work. it may be easier when the na list is sorted alphabetically
        waiting_for_obj = kmt.model.na_files.get_item(kmt.glob.WAITING_FOR_STR)
        self.next_actions_widget.set_active_item(waiting_for_obj.get_id())
        # self.next_actions_widget.coll_selection_qcb.setCurrentText(gtd.model.WAITING_FOR_STR)

    def on_open_email_clicked(self):
        email_str = kmt.glob.get_string_from_config(
            kmt.glob.SETTINGS_SECTION_EXTERNAL_TOOLS_STR,
            kmt.glob.SETTINGS_EMAIL_STR,
            "thunderbird"
        )
        kmt.glob.launch_string(email_str)

        # Idea: "mailto:"


class DestInputWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.dest_file_qsw = QtWidgets.QStackedWidget()
        vbox_l1.addWidget(self.dest_file_qsw)

        self.dest_text_file_qpte = QtWidgets.QPlainTextEdit()
        self.dest_file_qsw.addWidget(self.dest_text_file_qpte)

        self.dest_other_file_qll = QtWidgets.QLabel()
        self.dest_file_qsw.addWidget(self.dest_other_file_qll)

    def set_file_contents(self, i_source_path: str):
        if kmt.glob.get_df_type(i_source_path) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            self.dest_file_qsw.setCurrentWidget(self.dest_text_file_qpte)
            with open(i_source_path, "r") as file:
                contents_str = file.read()
                self.dest_text_file_qpte.setPlainText(contents_str)
        else:
            self.dest_file_qsw.setCurrentWidget(self.dest_other_file_qll)
        # self.dest_text_qpte.clear()


class RefProcCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.source_path_str = ""
        self.dest_path_str = ""

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.save_qpb = QtWidgets.QPushButton("Save")
        vbox_l1.addWidget(self.save_qpb)
        self.save_qpb.setFont(kmt.glob.get_title_font())
        self.save_qpb.clicked.connect(self.save_file)

        dest_hbox_l2 = QtWidgets.QHBoxLayout()
        vbox_l1.addLayout(dest_hbox_l2)

        self.dest_path_qll = QtWidgets.QLabel("-")
        dest_hbox_l2.addWidget(self.dest_path_qll, stretch=1)

        self.select_dest_qpb = QtWidgets.QPushButton("select dest (gtd)")
        dest_hbox_l2.addWidget(self.select_dest_qpb)
        self.select_dest_qpb.clicked.connect(self.on_select_dest_clicked)

        self.dest_file_qsw = QtWidgets.QStackedWidget()
        vbox_l1.addWidget(self.dest_file_qsw)

        self.dest_text_file_qpte = QtWidgets.QPlainTextEdit()
        self.dest_file_qsw.addWidget(self.dest_text_file_qpte)

        self.dest_other_file_qll = QtWidgets.QLabel()
        self.dest_file_qsw.addWidget(self.dest_other_file_qll)

    def set_source_path(self, i_source_path: str):
        self.source_path_str = i_source_path

        if kmt.glob.get_df_type(self.source_path_str) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            self.dest_file_qsw.setCurrentWidget(self.dest_text_file_qpte)
            with open(self.source_path_str, "r") as file:
                contents_str = file.read()
                self.dest_text_file_qpte.setPlainText(contents_str)
        else:
            self.dest_file_qsw.setCurrentWidget(self.dest_other_file_qll)
        # self.dest_text_qpte.clear()

    def set_dest(self, i_dest_path: str):
        # dir: moving file
        # file: editing or creating a text file
        self.dest_path_str = i_dest_path

    def save_file(self):
        if kmt.glob.get_df_type(self.dest_path_str) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            if not os.path.isfile(self.dest_path_str):
                with open(self.dest_path_str, "x") as file:
                    pass
            with open(self.dest_path_str, "w") as file:
                contents_str = self.dest_text_file_qpte.toPlainText()
                file.write(contents_str)
            self.dest_text_file_qpte.clear()
        else:
            kmt.glob.copy_fd(self.source_path_str, self.dest_path_str)

        # Resetting
        self.dest_path_str = ""
        self.dest_path_qll.setText(self.dest_path_str)

    def on_select_dest_clicked(self):
        gtd_main_dir_path_str = kmt.glob.get_path()
        if kmt.glob.get_df_type(self.source_path_str) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            path_sel_enum = kmt.widgets.path_sel_dlg.PathSelectionEnum.file
            file_name_str = os.path.basename(self.source_path_str)
            # file_name_str = ""
        else:
            path_sel_enum = kmt.widgets.path_sel_dlg.PathSelectionEnum.dir
            file_name_str = ""
        (dest_path_str, result_enum) = kmt.widgets.path_sel_dlg.PathSelDlg.open_dlg_and_get_path(
            path_sel_enum, gtd_main_dir_path_str, file_name_str, i_create_file_if_nonexistant=False)
        logging.debug(dest_path_str)
        logging.debug(result_enum)

        if result_enum != kmt.widgets.path_sel_dlg.PathSelectionEnum.cancelled:
            type_str = " [???]"
            if result_enum == kmt.widgets.path_sel_dlg.PathSelectionEnum.file:
                type_str = " [file]"
            elif result_enum == kmt.widgets.path_sel_dlg.PathSelectionEnum.dir:
                type_str = " [dir]"
            self.dest_path_qll.setText(dest_path_str + type_str)
            self.set_dest(dest_path_str)


class IncubateProcCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.source_id_str = ""

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.save_qpb = QtWidgets.QPushButton("Save")
        vbox_l1.addWidget(self.save_qpb)
        self.save_qpb.setFont(kmt.glob.get_title_font(True))
        self.save_qpb.clicked.connect(self.save_file)

        vbox_l1.addStretch(1)

        self.calendar_input_widget = kmt.widgets.calendar_input_dlg.CalendarCw(7)
        vbox_l1.addWidget(self.calendar_input_widget)

        # self.dest_input_widget = DestInputWidget()
        # vbox_l1.addWidget(self.dest_input_widget)

        self.dest_file_qsw = QtWidgets.QStackedWidget()
        vbox_l1.addWidget(self.dest_file_qsw)

        self.dest_text_file_qpte = QtWidgets.QPlainTextEdit()
        self.dest_file_qsw.addWidget(self.dest_text_file_qpte)

        self.dest_other_file_qll = QtWidgets.QLabel("non-text file")
        self.dest_file_qsw.addWidget(self.dest_other_file_qll)

        vbox_l1.addStretch(1)

    def set_file_contents(self, i_source_path: str):
        if kmt.glob.get_df_type(i_source_path) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            self.dest_file_qsw.setCurrentWidget(self.dest_text_file_qpte)
            with open(i_source_path, "r") as file:
                contents_str = file.read()
                self.dest_text_file_qpte.setPlainText(contents_str)
        else:
            self.dest_file_qsw.setCurrentWidget(self.dest_other_file_qll)
        # self.dest_text_qpte.clear()

    def set_source(self, i_source_id: str):
        self.source_id_str = i_source_id
        source_path_str = kmt.model.inbox_dir.get_item(i_source_id).get_path()
        self.set_file_contents(source_path_str)
        """
        source_path_str = gtd.model.inbox_dir.get_item(i_source_id).get_path()
        with open(source_path_str, "r") as file:
            contents_str = file.read()
            self.dest_text_qpte.setPlainText(contents_str)
        self.dest_text_qpte.clear()
        """

    def save_file(self):
        inbox_item = kmt.model.inbox_dir.get_item(self.source_id_str)
        source_path_str = inbox_item.get_path()
        datetime_str = self.calendar_input_widget.get_datetime_string()

        if kmt.glob.get_df_type(source_path_str) in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            contents_str = self.dest_text_file_qpte.toPlainText()
            kmt.model.tickler_files.add_tickler_note(datetime_str, contents_str)
            self.dest_text_file_qpte.clear()
        else:
            kmt.glob.add_tickler_file(datetime_str, source_path_str, False)

        self.calendar_input_widget.reset_datetime()

