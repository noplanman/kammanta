import logging
import os
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import kmt.model
import kmt.widgets.checklist_cw
import kmt.widgets.calendar_input_dlg
import kmt.glob
import kmt.gtd_info

TITLE_STR = "Processing"


class ProcessingDlg(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()

        self.setMinimumWidth(500)
        self.setMinimumHeight(400)

        self.setSizeGripEnabled(True)
        self.setWindowTitle("Processing")

        hbox_l2 = QtWidgets.QHBoxLayout(self)
        self.setLayout(hbox_l2)

        new_font = QtGui.QFont()
        new_font.setPointSize(14)

        left_vbox_l3 = QtWidgets.QVBoxLayout()
        hbox_l2.addLayout(left_vbox_l3)

        self.inbox_item_title_qll = QtWidgets.QLabel("Inbox item")
        left_vbox_l3.addWidget(self.inbox_item_title_qll)
        # self.inbox_item_title_qll.setText(i_source_path)

        self.actions_qbg = QtWidgets.QButtonGroup()
        self.actions_qbg.setExclusive(True)
        self.actions_qbg.buttonClicked.connect(self.on_action_button_clicked)

        actions_hbox_l4 = QtWidgets.QHBoxLayout()
        left_vbox_l3.addLayout(actions_hbox_l4)

        self.trash_qpb = QtWidgets.QPushButton("Trash")
        actions_hbox_l4.addWidget(self.trash_qpb)
        self.trash_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.trash_qpb)

        self.ref_qpb = QtWidgets.QPushButton("Ref")
        actions_hbox_l4.addWidget(self.ref_qpb)
        self.ref_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.ref_qpb)

        self.incubate_qpb = QtWidgets.QPushButton("Incubate")
        actions_hbox_l4.addWidget(self.incubate_qpb)
        self.incubate_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.incubate_qpb)

        self.do_qpb = QtWidgets.QPushButton("Do")
        actions_hbox_l4.addWidget(self.do_qpb)
        self.do_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.do_qpb)

        self.na_qpb = QtWidgets.QPushButton("New NA")
        actions_hbox_l4.addWidget(self.na_qpb)
        self.na_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.na_qpb)

        self.prj_qpb = QtWidgets.QPushButton("New Prj")
        actions_hbox_l4.addWidget(self.prj_qpb)
        self.prj_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.prj_qpb)

        self.defer_qpb = QtWidgets.QPushButton("Defer")
        actions_hbox_l4.addWidget(self.defer_qpb)
        self.defer_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.defer_qpb)

        self.delegate_qpb = QtWidgets.QPushButton("Delegate")
        actions_hbox_l4.addWidget(self.delegate_qpb)
        self.delegate_qpb.setCheckable(True)
        self.actions_qbg.addButton(self.delegate_qpb)

        hbox_l4 = QtWidgets.QHBoxLayout()
        left_vbox_l3.addLayout(hbox_l4)

        self.source_path_qll = QtWidgets.QLabel("-")
        hbox_l4.addWidget(self.source_path_qll, stretch=1)

        self.delete_source_on_exit_qcb = QtWidgets.QCheckBox("del on exit")
        hbox_l4.addWidget(self.delete_source_on_exit_qcb)
        self.delete_source_on_exit_qcb.setChecked(False)
        # i_delete_source_on_exit

        self.left_qsw = QtWidgets.QStackedWidget()
        left_vbox_l3.addWidget(self.left_qsw)

        self.source_qpte = QtWidgets.QPlainTextEdit()
        self.left_qsw.addWidget(self.source_qpte)
        self.source_qpte.setFont(new_font)
        self.source_qpte.setReadOnly(True)

        self.source_image_qll = QtWidgets.QLabel()
        self.left_qsw.addWidget(self.source_image_qll)

        # right side..

        right_vbox_l3 = QtWidgets.QVBoxLayout()
        hbox_l2.addLayout(right_vbox_l3)

        hbox_l4 = QtWidgets.QHBoxLayout()
        right_vbox_l3.addLayout(hbox_l4)

        self.right_qsw = QtWidgets.QStackedWidget()
        right_vbox_l3.addWidget(self.right_qsw)

        # ..widgets on the stack

        self.empty_qll = QtWidgets.QLabel("Please select a processing type")
        self.right_qsw.addWidget(self.empty_qll)

        self.do_widget = DoCw()
        self.right_qsw.addWidget(self.do_widget)

        self.trash_qll = QtWidgets.QLabel("Deleting")
        self.right_qsw.addWidget(self.trash_qll)

        self.defer_widget = DeferCw()
        self.right_qsw.addWidget(self.defer_widget)

        self.ref_dest = RefDest()
        self.right_qsw.addWidget(self.ref_dest)

        self.next_actions_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.na_files)
        self.right_qsw.addWidget(self.next_actions_column)

        self.projects_column = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.prj_fds)
        self.right_qsw.addWidget(self.projects_column)

        self.incubate_widget = IncubateCw()
        self.right_qsw.addWidget(self.incubate_widget)

        self.delegate_widget = DelegateCw()
        self.right_qsw.addWidget(self.delegate_widget)

        self.right_qsw.setCurrentWidget(self.empty_qll)

        # TODO save button or similar

        self.update_gui()

    def set_inbox_source_id(self, i_id: str):
        inbox_item_obj = kmt.model.inbox_dir.get_item(i_id)
        source_path_str = inbox_item_obj.get_path()
        self.source_path_qll.setText(source_path_str)

        # Checking the type of file that we have
        df_type = kmt.glob.get_df_type(source_path_str)

        if df_type in (kmt.glob.EntityType.note_file, kmt.glob.EntityType.text_file):
            try:
                with open(source_path_str, "r") as file:
                    self.source_qpte.setPlainText(file.read())
            except UnicodeDecodeError:
                pass
            self.left_qsw.setCurrentWidget(self.source_qpte)
        elif df_type in (kmt.glob.EntityType.image_file,):
            pixmap = QtGui.QPixmap(source_path_str)
            pixmap = pixmap.scaled(
                340, 340,
                QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation
            )
            self.source_image_qll.setPixmap(pixmap)
            self.left_qsw.setCurrentWidget(self.source_image_qll)

        self.update_gui()

    def on_action_button_clicked(self, i_button):
        self.update_gui()

    # overridden
    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        pass
        # self.update_gui()
        # -removed for now since this has been seen (only once) to cause an infinite loop

    def update_gui(self):
        if self.do_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.do_widget)
        elif self.trash_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.trash_qll)
        elif self.ref_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.ref_dest)
        elif self.na_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.next_actions_column)
            self.next_actions_column.update_gui_and_ids()
        elif self.prj_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.projects_column)
            self.projects_column.update_gui_and_ids()
        elif self.defer_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.defer_widget)
        elif self.delegate_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.delegate_widget)
        elif self.incubate_qpb.isChecked():
            self.right_qsw.setCurrentWidget(self.incubate_widget)
        else:
            pass


class RefDest(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l1.addLayout(hbox_l4)

        self.dest_path_qll = QtWidgets.QLabel("-")
        hbox_l4.addWidget(self.dest_path_qll, stretch=1)

        self.select_dest_qpb = QtWidgets.QPushButton("select dest (gtd)")
        hbox_l4.addWidget(self.select_dest_qpb)
        self.select_dest_qpb.clicked.connect(self.on_select_dest_clicked)

        self.select_dest_gen_qpb = QtWidgets.QPushButton("select dest (gen ref)")
        hbox_l4.addWidget(self.select_dest_gen_qpb)

        self.dest_text_qpte = QtWidgets.QPlainTextEdit()
        hbox_l4.addWidget(self.dest_text_qpte)

        # hbox_l2.addWidget(self.dest_qpte)
        # self.dest_text_qpte.setFont(new_font)

    def on_select_dest_clicked(self):
        gtd_main_dir_path_str = kmt.glob.get_path()
        (dest_str, result_bool) = QtWidgets.QFileDialog.getSaveFileName(
                self, "caption", gtd_main_dir_path_str)
        logging.debug(dest_str)
        logging.debug(result_bool)
        # QtWidgets.QFileDialog.getOpenFileName()
        if result_bool:
            # self.right_qsw.setCurrentWidget(self.dest_text_qpte)
            self.dest_path_qll.setText(dest_str)
            if not os.path.isfile(dest_str):
                with open(dest_str, "x") as file:
                    pass
            with open(dest_str, "r") as file:
                self.dest_text_qpte.setPlainText(file.read())


class DoCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.do_qll = QtWidgets.QLabel("Do if less than x minutes")
        vbox_l1.addWidget(self.do_qll)


class DeferCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        vbox_l1.addStretch(1)

        self.defer_qll = QtWidgets.QLabel("Schdule for another time")
        vbox_l1.addWidget(self.defer_qll)

        vbox_l1.addStretch(1)

        self.open_calendar_qpb = QtWidgets.QPushButton("Open Calendar")
        vbox_l1.addWidget(self.open_calendar_qpb)
        self.open_calendar_qpb.clicked.connect(self.on_open_calendar_clicked)

        vbox_l1.addStretch(1)

    def on_open_calendar_clicked(self):
        calendar_str = kmt.glob.get_string_from_config(
            kmt.glob.SETTINGS_SECTION_EXTERNAL_TOOLS_STR,
            kmt.glob.SETTINGS_CALENDAR_STR,
            "https://calendar.google.com/calendar/r/week"
        )
        kmt.glob.launch_string(calendar_str)


class DelegateCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        self.open_email_qpb = QtWidgets.QPushButton("Open email")
        vbox_l1.addWidget(self.open_email_qpb)
        self.open_email_qpb.clicked.connect(self.on_open_email_clicked)

        self.next_actions_widget = kmt.widgets.checklist_cw.ChecklistWidget(kmt.model.na_files)
        vbox_l1.addWidget(self.next_actions_widget)

        # TODO: making this work. it may be easier when the na list is sorted alphabetically
        waiting_for_obj = kmt.model.na_files.get_item(kmt.glob.WAITING_FOR_STR)
        self.next_actions_widget.set_active_item(waiting_for_obj.get_id())
        # self.next_actions_widget.coll_selection_qcb.setCurrentText(gtd.model.WAITING_FOR_STR)

    def on_open_email_clicked(self):
        email_str = kmt.glob.get_string_from_config(
            kmt.glob.SETTINGS_SECTION_EXTERNAL_TOOLS_STR,
            kmt.glob.SETTINGS_EMAIL_STR,
            "thunderbird"
        )
        kmt.glob.launch_string(email_str)


class IncubateCw(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l1)

        vbox_l1.addStretch(1)

        self.calendar_input_widget = kmt.widgets.calendar_input_dlg.CalendarCw(7)
        vbox_l1.addWidget(self.calendar_input_widget)

        vbox_l1.addStretch(1)

        # self.calendar_input_widget

