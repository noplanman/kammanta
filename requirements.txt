PyQt5
# -5.14 needed for markdown support
PyQt5-sip
# Version histories:
# https://pypi.org/project/PyQt5/#history
# Changes/updates for new Qt versions: https://wiki.qt.io/Category:Release
# PyQt5-stubs
# https://pypi.org/project/PyQt5-stubs/
