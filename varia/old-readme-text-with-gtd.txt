
### What is GTD©?

*GTD* is a productivity and life management system, which helps you get to "relaxed productivity"

#### GTD and positive psychology

GTD can help you
* focus on what is important
* stay on track with projects
* relax even when there are many things to do

> Being creative, strategic, and simply present and loving don’t require time—they require space.​

-- David Allen

